// ZorrocTraderFIX.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

#include "ZorroTraderFIX.h"

#include <chrono>
#include <iostream>
#include <map>
#include <thread>

#include <quickfix/Initiator.h>
#include <quickfix/FileStore.h>
#include <quickfix/FileLog.h>
#include <quickfix/fix44/MarketDataRequest.h>
#include <quickfix/fix44/MarketDataSnapshotFullRefresh.h>
#include <quickfix/Log.h>
#include <quickfix/Settings.h>
#include <quickfix/SessionSettings.h>
#include <quickfix/ThreadedSSLSocketInitiator.h>

static const char pluginName[] = "CTFIX";
int(__cdecl *BrokerError)(const char* txt) = NULL;
int(__cdecl *BrokerProgress)(const int percent) = NULL;
static bool initialized = false;
static FIX::Dictionary default_dict;

void TestApp::onCreate(const SessionID&) {};
void TestApp::onLogon(const SessionID& session_id) {
	const std::string& senderCompId = session_id.getSenderCompID().getValue();
	//BrokerError((std::string("Logon - ")).append(senderCompId).c_str());
	loggedInSessions[senderCompId] = true;
	compIdToSession[senderCompId] = session_id;
};
void TestApp::onLogout(const SessionID& session_id) {
	//BrokerError((std::string("Logout - ")).append(session_id.getSenderCompID().getValue()).c_str());
	loggedInSessions[session_id.getSenderCompID().getValue()] = false;
};
void TestApp::toAdmin(Message& message, const SessionID& session_id) {
	FIX::TargetSubID targetSubId("QUOTE");
	message.setField(targetSubId);

	MsgType msg_type;
	message.getHeader().getField(msg_type);

	if (msg_type == FIX::MsgType_Logon) {
		message.setField(FIX::EncryptMethod(FIX::EncryptMethod_NONE));
		message.setField(FIX::ResetSeqNumFlag(FIX::ResetSeqNumFlag_YES));
		const std::string& senderCompId = session_id.getSenderCompID().getValue();
		const auto& login = logins.find(senderCompId);
		if (login != logins.end()) {
			message.setField(FIX::Username(login->second.first));
			message.setField(FIX::Password(login->second.second));
		}
	}
};

void TestApp::toApp(Message& message, const SessionID& session_id)  throw(FIX::DoNotSend) {
};
void TestApp::fromAdmin(const Message& message, const SessionID& session_id)  throw(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::RejectLogon) {
	crack(message, session_id);
};
void TestApp::fromApp(const Message& message, const SessionID& session_id)  throw(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::UnsupportedMessageType) {
	crack(message, session_id);
};
void TestApp::onMessage(const FIX44::MarketDataSnapshotFullRefresh& message, const SessionID& session_id) throw(FIX::FieldNotFound&, FIX::IncorrectDataFormat&, FIX::IncorrectTagValue&, FIX::UnsupportedMessageType&) {
	FIX::Symbol symbol;
	message.getField(symbol);

	FIX44::MarketDataSnapshotFullRefresh::NoMDEntries group;
	FIX::MDEntryPx entry_px;
	FIX::MDEntryType entry_type;
	double bid, ask;

	message.getGroup(1, group);
	group.get(entry_px);
	group.get(entry_type);

	if (entry_type == MDEntryType_BID) {
		bid = entry_px.getValue();
	}
	else {
		ask = entry_px.getValue();
	}

	message.getGroup(2, group);
	group.get(entry_px);
	group.get(entry_type);

	if (entry_type == MDEntryType_BID) {
		bid = entry_px.getValue();
	}
	else {
		ask = entry_px.getValue();
	}
	const std::string& senderCompId = session_id.getSenderCompID().getValue();
	const std::string& symString = symbol.getValue();
	currentPrices[senderCompId][symString].first = bid;
	currentPrices[senderCompId][symString].second = ask;
	if (subscribing_symbol == symString && subscribing_account == senderCompId) {
		{
			std::unique_lock<std::mutex> lock(subscribing_mutex);
			subscribing = false;
		}
		subscribecv.notify_all();
	}
}

void TestApp::subscribe(const std::string& symbol) {
	std::string brokerPrefix, symbolName;
	std::istringstream symbolstream(symbol);
	std::getline(symbolstream, brokerPrefix, '-');
	std::getline(symbolstream, symbolName, '-');
	if (symbolPrefixBrokerMap.find(brokerPrefix) == symbolPrefixBrokerMap.end()) {
		symbolPrefixBrokerMap[brokerPrefix] = currentAccount;
	} else {
		currentAccount = symbolPrefixBrokerMap[brokerPrefix];
	}
	const auto& thing = compIdToSession.find(currentAccount);
	if (thing == compIdToSession.end()) {
		BrokerError(("Couldn't find session " + currentAccount + "\n").c_str());
		return;
	}
	BrokerError(("Subscribing to asset " + symbol + " on account " + currentAccount + "\n").c_str());
	return subscribe_session(symbolName, compIdToSession[currentAccount]);
}

void TestApp::subscribe_session(const std::string& symbol, const SessionID& session_id) {
	BrokerError(("Sending sub request for " + symbol + "\n'").c_str());
	FIX44::MarketDataRequest message(
		FIX::MDReqID(genSubID()),
		FIX::SubscriptionRequestType(FIX::SubscriptionRequestType_SNAPSHOT_PLUS_UPDATES),
		1
	);

	FIX44::MarketDataRequest::NoMDEntryTypes bidGroup;
	bidGroup.set(FIX::MDEntryType(FIX::MDEntryType_BID));
	message.addGroup(bidGroup);

	FIX44::MarketDataRequest::NoMDEntryTypes askGroup;
	askGroup.set(FIX::MDEntryType(FIX::MDEntryType_OFFER));
	message.addGroup(askGroup);

	FIX44::MarketDataRequest::NoRelatedSym symbolGroup;
	symbolGroup.set(FIX::Symbol(symbol));
	message.addGroup(symbolGroup);
	{
		std::unique_lock<std::mutex> lock(subscribing_mutex);
		subscribing_symbol = symbol;
		subscribing_account = session_id.getSenderCompID().getValue();
		subscribing = true;
	}
	FIX::Session::sendToTarget(message, session_id);
	BrokerError("Waiting for subscribe CV\n");
	{
		std::unique_lock<std::mutex> lock(subscribing_mutex);
		while (subscribing) {
			subscribecv.wait(lock);
		}
	}
	return;
}

bool TestApp::getPrices(const std::string& symbol, double* spread, double* ask) {
	std::string brokerPrefix, symbolName;
	std::istringstream symbolstream(symbol);
	std::getline(symbolstream, brokerPrefix, '-');
	std::getline(symbolstream, symbolName, '-');
	if (symbolPrefixBrokerMap.find(brokerPrefix) == symbolPrefixBrokerMap.end()) {
		symbolPrefixBrokerMap[brokerPrefix] = currentAccount;
	}
	if (symbolPrefixBrokerMap.find(brokerPrefix) == symbolPrefixBrokerMap.end()) {
		BrokerError("Broker Prefix not found\n");
		return false;
	} else {
		currentAccount = symbolPrefixBrokerMap[brokerPrefix];
	}
	if (currentAccount.empty()) {
		BrokerError("CurrentAccount empty\n");
		return false;
	}
	const auto& accountPrices = currentPrices.find(currentAccount);
	if (accountPrices == currentPrices.end()) {
		BrokerError("Prices for account not found.");
		return false;
	}
	const auto& symbolPrices = accountPrices->second.find(symbolName);
	if (symbolPrices == accountPrices->second.end()) {
		BrokerError("Symbol not found\n");
		return false;
	}
	*spread = symbolPrices->second.second - symbolPrices->second.first;
	*ask = symbolPrices->second.second;
	return true;
}

void TestApp::run() {
	while (true) {
		FIX::process_sleep(1);
	}
}

bool TestApp::setAccount(const std::string& account) {
	if (loggedInSessions.find(account) == loggedInSessions.end()) {
		BrokerError(("Account not valid: " + account + "\n").c_str());
		return false;
	}
	currentAccount = account;
	BrokerError(("Account now: " + account + "\n").c_str());
	return true;
}

std::unique_ptr<TestApp> app;
Initiator *initiator;

int init(char* accounts) {
	initialized = true;
	BrokerError("Loading session settings\n");
	SessionSettings settings("Plugin\\ctfix.ini");
	BrokerError("Session settings loaded\n");

	std::map<std::string, std::pair<std::string, std::string>> logins;
	std::string accounts_string;
	int account_string_offset = 0;
	for (const SessionID& session : settings.getSessions()) {
		const Dictionary& session_dict = settings.get(session);
		const std::string& senderCompId = session.getSenderCompID().getValue();
		logins[senderCompId] = std::pair<std::string, std::string>(
			session_dict.getString("Username"),
			session_dict.getString("Password"));
	}
	//TODO: Add trade session.
	std::stringstream msg;
	msg << "Found " << logins.size() << " accounts\n";
	BrokerError(msg.str().c_str());

	app.reset(new TestApp(logins));

	try {
		FileStoreFactory storeFactory(settings);
		FileLogFactory logFactory(settings);
		initiator = new ThreadedSSLSocketInitiator(*app, storeFactory, settings, logFactory);
	}
	catch (const std::exception& e) {
		BrokerError(e.what());
		return 0;
	}
	BrokerError("Starting initiator\n");
	initiator->start();
	BrokerError("Initiator started\n");
	for (int waitloops = 20; waitloops >= 0; waitloops--) {
		if (app->isLoggedIn()) {
			BrokerError("Logged in!\n");
			return 1;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
		BrokerError("Not logged in yet...\n");
	}
	BrokerError("Login failed\n");
	return 0;
}

int BrokerOpen(char *Name, FARPROC fpError, FARPROC fpProgress) {
	strcpy_s(Name, 32, pluginName);
	(FARPROC&)BrokerError = fpError;
	(FARPROC&)BrokerProgress = fpProgress;
	return 2;
}

int BrokerLogin(char* User, char* Pwd, char* Type, char* Accounts) {
	if (User != nullptr) {
		BrokerError("Logging in\n");
		if (!initialized) {
			BrokerError("Not initialized, logging in\n");
			return init(Accounts);
		}
		strcpy_s(Accounts, 1024, User);
		app->setAccount(User);
		return app->isLoggedIn() ? 1 : 0;
	}
	else {
		BrokerError("Logging out...\n");
		initiator->stop();
	}
}

int BrokerTime(DATE *pTimeUTC) {
	return 2;
}

int BrokerAsset(char* Asset, double *pPrice, double *pSpread, double *pVolume, double *pPip, double *pPipCost, double *pLotAmount, double *pMarginCost, double *pRollLong, double *pRollShort) {
	//BrokerError(("BrokerAsset for " + std::string(Asset) + "\n").c_str());
	try {
		if (!app->getPrices(Asset, pSpread, pPrice)) {
			app->subscribe(Asset);
		}
		return 1;
	}
	catch (const std::exception& e) {
		BrokerError(e.what());
		return 0;
	}
}

int BrokerAccount(char* Account, double *pBalance, double *pTradeVal, double *pMarginVal) {
	if (Account == nullptr) {
		return 1;
	}
	else {
		return app->setAccount(Account) ? 1 : 0;
	}
}