# Compilation

You'll need:

- [QuickFix](http://quickfixengine.org/): This is the fix engine that does all the work. My plugin is a shitty wrapper around this FIX engine. You can download binaries but they won't have SSL support. 
- [OpenSSL](https://www.openssl.org/): This is optional. If you don't need SSL support, you don't need it. This project is configured assuming you do have it and that you're using [this installer](https://slproweb.com/products/Win32OpenSSL.html) with its defaults.

Once you've got them, just open the solution with VS2017 and compile. I'm not a Windows person, I don't know better ways to do this I'm afraid.

# Usage

In addition to the output DLL, you'll need two files:

- `ctfix.ini`, which goes in Zorro's `Plugin` directory next to the DLL. It's a standard [QuickFix configuration file](http://www.quickfixengine.org/quickfix/doc/html/configuration.html). An example is included.
- `FIX44.xml`, which goes in the same place. Download it from the QuickFix homepage listed above.

There are currently some caveats you need to keep in mind:

- The FIX API only allows one connection per account, so you can't run two Zorros on the same account.
- The plugin will connect to _all_ configured brokers in `ctfix.ini` on the first call to `BrokerLogin`.
- Since I wanted multi-account support in my plugin and the BrokerAccount API doesn't seem to do anything, _all_ your configured symbols, regardless of whether you're using multiple accounts or not, _must_ be prefixed with a consistent prefix, of the form "prefix-symbol".
- cTrader's FIX API uses numeric IDs for all assets. A mapping is included in AssetsCT.csv (which does not have the aforementioned prefix).
- This is very much beta. It will likely break sometimes and not in others. 
- **Currently trading is not implemented**